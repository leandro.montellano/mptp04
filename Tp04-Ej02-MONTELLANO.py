import os

def menu():
    print('=========== MENU PRINCIPAL ===========')
    print('a.Registrar un producto.')
    print('b.Mostrar el listado de productos.')
    print('c.Mostrar los productos cuyo stock se encuentre en un intervalo.')
    print('d.Sumar X a stock de todos los productos.')
    print('e.Eliminar todos los productos con stock nulo.')
    print('f.Salir')
    print('=======================================')
    while True:
        opc = input('Seleccione una opcion valida:\n')
        if opc in ['a', 'b', 'c', 'd', 'e','f']:
            return opc

def validarStock():
    while True:
        try:
            n = int(input('Ingrese Stock del producto: \n'))
            while (n<0):
                n = int(input('Error, ingrese Stock valido: \n'))
            return n
        except ValueError:
            print('Error, ingrese Stock valido:')
            continue

def validarPrecio():
    while True:
        try:
            n = float(input('Ingrese precio del producto: \n'))
            while (n<0):
                n = float(input('Error, ingrese un precio valido: \n'))
            return n
        except ValueError:
            print('Error, ingrese un precio valido:')
            continue

def validarCodigo():
    #Valida que el codigo sea un numero
    bandera=-1   
    while True:
        try:
            n = int(input('Ingrese el codigo del producto: \n'))
            while (n<0):
                n = int(input('Error, ingrese un codigo valido: \n'))
            return n           
        except ValueError:
            print('Error, ingrese un codigo valido:')
            continue 

def registrarProducto(agregarProducto):
    print('=============== REGISTRANDO PRODUCTO ===============')
    codigoDeProducto=validarCodigo()
    descripcion=input('Ingrese descripcion del producto: \n')
    precio=validarPrecio()
    stock=validarStock()
    agregarProducto[codigoDeProducto]= [descripcion, precio, stock]
    
def mostrarProductos(productos):
    
    print('==================== PRODUCTOS REGISTRADOS ====================')   
    for clave, valor in productos.items():
        print('Codigo de producto: ',clave,' Descripcion: ', valor[0],' Precio: ', valor[1],' Stock: ',valor[2])
    if(len(productos)==0):
        print('No hay productos registrados.')
    input('\nPresione una tecla para continuar.')

def eliminarStock(productos):
    print('============== PRODUCTOS CON STOCK NULO ELIMINADOS. ==============')
    for clave,valor in list(productos.items()):
        if(valor[2]==0):            
            del productos[clave]                
    input('\nPresione una tecla para continuar.')        

def validarNumero(mensaje):
    while True:
        try:
            n = int(input(mensaje))
            return n
        except ValueError:
            print('Error, debe ingresar un numero entero.')
            continue

def mostrarRango(productos):
    valor1=validarNumero('Ingrese un valor 1:\n')
    valor2=validarNumero('Ingrese un valor 2:\n')
    bandera=False
    print('================== STOCK ENCONTRADOS ENTRE ESOS VALORES ==================')
    for clave, valor in productos.items():
        if(valor[2]>=valor1 and valor[2]<=valor2):
            print('Codigo de producto: ',clave,' Descripcion: ', valor[0],' Precio: ', valor[1],' Stock: ',valor[2])
            bandera=True
    if(bandera==False):
        print('No se encontraron productos. ')
    input('\nPresione una tecla para continuar.')

def sumarX(productos):
    print('\n=== Ingrese un valor Y, para sumarle un valor X a todos los productos con STOCK menores o iguales a Y ===')
    X=validarNumero('Ingrese el valor X:')
    Y=validarNumero('Ingrese el valor Y:')
    i=0
    for clave,valor in list(productos.items()):
        if(valor[2]<=Y):
            i+=1
            n=valor[2]
            productos[clave]=[valor[0],valor[1],n+X]           
    print('Se han modificado ',i,' productos.') 
    input('\nPresione una tecla para continuar.')

#Principal

productos={}
opcion='a'
while(opcion != 'f'):
    os.system('cls')
    opcion=menu()
    if(opcion=='a'):
        registrarProducto(productos)
    elif(opcion=='b'):
        mostrarProductos(productos)
    elif(opcion=='c'):
        mostrarRango(productos)
    elif(opcion=='d'):
        sumarX(productos)
    elif(opcion=='e'):
        eliminarStock(productos)